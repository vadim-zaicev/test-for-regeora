import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'

import App from "./App";


export default combineReducers({
    routing: routerReducer,
    App
});