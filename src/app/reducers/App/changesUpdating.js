import {constants} from "../../actions/constants";

export function changesUpdating(state = false, action = {}) {
    switch(action.type) {
        case constants.APP.SAVE_CHANGES_CLICKED:
            return true;
        case constants.APP.CHANGES_SAVED:
        case constants.APP.LAYERS_LOADED:
            return false;
        default:
            return state;
    }
}