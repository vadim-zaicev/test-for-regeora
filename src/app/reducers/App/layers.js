import {constants} from "../../actions/constants";

export function layers(state = [], action = {}) {
    switch(action.type) {

        case constants.APP.LAYERS_LOADED:
            console.log('layers loaded in reducer', action.value);
            return [].concat(action.value);

        case constants.APP.LAYER_SELECTED:
            return state.map(layer => {
               if (action.value.id === layer.id) return Object.assign({}, layer, {current: true});
               else if (layer.current) return Object.assign({}, layer, {current: false});
               return layer;
            });

        case constants.APP.LAYER_ADDED:
            return [...state, action.value];

        case constants.APP.DELETE_LAYER_CLICKED:
            const removed_index = state.indexOf(action.value);

            return [
                ...state.slice(0, removed_index),
                ...state.slice(removed_index+1)
            ];

        case constants.APP.LAYER_UPDATED:
            const changed_index = state.indexOf(action.value.layer);

            return [
                ...state.slice(0, changed_index),
                Object.assign({}, action.value.layer, action.value.change),
                ...state.slice(changed_index+1)
            ];


        case constants.APP.EDIT_MODE_TOGGLED:
            const toggled_index = state.indexOf(action.value);

            return [
                ...state.slice(0, toggled_index).map(l => l.editMode ? Object.assign({},l, {editMode: false}) : l),
                Object.assign({}, action.value, {editMode: !action.value.editMode}),
                ...state.slice(toggled_index+1).map(l => l.editMode ? Object.assign({},l, {editMode: false}) : l)
            ];


        case constants.APP.MARKER_REMOVED:
            const current = state.find(l => l.current);
            const current_index = state.indexOf(current);
            const marker = current.markers.find(m => m.lat === action.value.lat && m.lng == action.value.lng);
            const marker_index = current.markers.indexOf(marker);

            return [...state.slice(0, current_index),
                Object.assign({}, current, {markers: [
                    ...current.markers.slice(0, marker_index),
                    ...current.markers.slice(marker_index+1)
                ]}),
                ...state.slice(current_index+1)
            ];


        case constants.APP.MARKER_CREATED:
            return state.map(layer => {
               if (!layer.current) return layer;
               return Object.assign(
                   {}, layer, {
                       markers: [
                               ...layer.markers,
                               {lat: action.value.lat, lng: action.value.lng, saved: false}
                           ]
                   });
            });

        default:
            return state;
    }
}