import {constants} from "../../actions/constants";

export function currentLayerSaved(state = true, action = {}) {
    switch(action.type) {
        case constants.APP.MARKER_CREATED:
        case constants.APP.MARKER_REMOVED:
            return false;
        case constants.APP.SAVE_CHANGES_CLICKED:
        case constants.APP.LAYER_SELECTED:
            return true;
        default:
            return state;
    }
}