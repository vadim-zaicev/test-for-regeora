import {constants} from "../../actions/constants";

export function loading(state = true, action = {}) {
    switch(action.type) {
        case constants.APP.LAYERS_LOADED:
            return false;
        default:
            return state;
    }
}