import {constants} from "../../actions/constants";

export function layerAdding(state = false, action = {}) {
    switch(action.type) {
        case constants.APP.LAYER_STARTED_ADDING:
            return true;
        case constants.APP.LAYER_ADDED:
            return false;
        default:
            return state;
    }
}