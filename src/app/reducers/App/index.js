import { combineReducers } from 'redux';
import { layers } from "./layers";
import { loading } from "./loading";
import { layerName } from "./layerName";
import { layerAdding } from "./layerAdding";
import { changesUpdating } from "./changesUpdating";
import { currentLayerSaved } from "./currentLayerSaved";


export default combineReducers({
    layers,
    loading,
    layerName,
    layerAdding,
    changesUpdating,
    currentLayerSaved
});