import {constants} from "../../actions/constants";

export function layerName(state = '', action = {}) {
    switch(action.type) {
        case constants.APP.LAYER_NAME_CHANGED:
            return action.value;
        case constants.APP.LAYER_ADDED:
            return '';
        default:
            return state;
    }
}