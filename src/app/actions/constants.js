const constants = {
    APP: {
        LAYERS_LOADED: null,
        LAYER_UPDATED: null,
        LAYERS_UPDATED: null,
        LAYER_ADDED: null,
        LAYER_STARTED_ADDING: null,
        LAYER_NAME_CHANGED: null,
        LAYER_SELECTED: null,
        MARKER_CREATED: null,
        SAVE_CHANGES_CLICKED: null,
        CHANGES_LOADED: null,
        MARKER_REMOVED: null,
        DELETE_LAYER_CLICKED: null,
        EDIT_MODE_TOGGLED: null,
    }
};

function updateKey(obj, key, value) {

    if (obj[key] === null) {
        return value;
    }

    if (typeof obj[key] === 'string') {
        return obj[key];
    }

    const answer = {};

    for (const innerKey in obj[key]) {
        answer[innerKey] = updateKey(obj[key], innerKey, value + '_' + innerKey);
    }

    return answer;

}

for (const key in constants) {
    constants[key] = updateKey(constants, key, key);
}

export { constants };