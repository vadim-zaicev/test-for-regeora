import { constants } from "./constants";
import {ToasterHelper} from "../helpers/ToasterHelper";
import {Intent} from "@blueprintjs/core";
import {LayerService} from "../services/LayerService";

export function layersLoaded(layers) {
    return {
        type: constants.APP.LAYERS_LOADED,
        value: layers
    }
}

export function layerAdded(layer) {
    return {
        type: constants.APP.LAYER_ADDED,
        value: layer
    }
}

export function markerRemoved(marker) {
    return {
        type: constants.APP.MARKER_REMOVED,
        value: marker
    }
}

export function deleteLayerClicked(layer) {

    return function(dispatch) {

        ToasterHelper.show({intent: Intent.WARNING, message: `Слой удаляется...`});

        LayerService.removeLayer(layer)
            .then(() => {

                ToasterHelper.show({intent: Intent.SUCCESS, message: `Слой удален.`});

                dispatch({
                    type: constants.APP.DELETE_LAYER_CLICKED,
                    value: layer
                });
            });
    }

}

export function saveChangesClicked(layers) {
    return function(dispatch) {
        dispatch({type: constants.APP.SAVE_CHANGES_CLICKED});

        ToasterHelper.show({intent: Intent.WARNING, message: `Изменения сохраняются...`});

        const newLayers = layers.map(l => Object.assign({}, l));

        newLayers.forEach(layer => {
            layer.markers = layer.markers.map(m => Object.assign({}, m, {saved: true}));
            layer.editMode = false;
        });

        LayerService.saveChanges(newLayers)
            .then(() => {
                ToasterHelper.show({intent: Intent.SUCCESS, message: `Изменения успешно сохранены.`});

                dispatch(layersLoaded(newLayers));

            });
    }
}

export function editModeToggled(value) {
    return {
        type: constants.APP.EDIT_MODE_TOGGLED,
        value
    }
}

export function layerUpdated(value) {
    return {
        type: constants.APP.LAYER_UPDATED,
        value
    }
}

export function layerNameChanged(layerName) {
    return {
        type: constants.APP.LAYER_NAME_CHANGED,
        value: layerName
    }
}

export function layerSelected(layer) {
    return function(dispatch) {

        LayerService.getLayers() //здесь лишний запрос к API, в продакшен версии я бы хранил предыдущие маркеры отдельно, чтобы восстановить их после переключения соя
        .then(layers => {
            dispatch(layersLoaded(layers));
            dispatch({type: constants.APP.LAYER_SELECTED, value: layer});
        });

    }

}

export function markerCreated(latlng) {
    return {
        type: constants.APP.MARKER_CREATED,
        value: latlng
    }
}

export function layerStartedAdding() {
    return {
        type: constants.APP.LAYER_STARTED_ADDING
    }
}

export function changesLoaded() {
    return {
        type: constants.APP.CHANGES_LOADED
    }
}

export function addLayer(layer) {
    return function(dispatch) {

        ToasterHelper.show({intent: Intent.WARNING, message: `Слой ${layer.name} создается...`});

        dispatch(layerStartedAdding());

        LayerService.addLayer(layer)
            .then(layerId => {

                ToasterHelper.show({intent: Intent.SUCCESS, message: `Слой ${layer.name} успешно добавлен.`});

                dispatch(layerAdded(Object.assign({},layer, {id: layerId})));

            });
    }

}