import { Position, Toaster } from "@blueprintjs/core";

export class ToasterHelper {

    static toaster;

    static show(params) {
        if (!ToasterHelper.toaster) {
            ToasterHelper.toaster = Toaster.create({
                position: Position.BOTTOM,
            });
        }

        ToasterHelper.toaster.show(params);

    }


}