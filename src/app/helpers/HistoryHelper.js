export class HistoryHelper {
    static history;

    static setHistory(history) {
        HistoryHelper.history = history;
    }

    static push(pathname) {
        if (HistoryHelper.history) HistoryHelper.history.push(pathname);
    }

}