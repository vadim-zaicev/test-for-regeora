import L from "leaflet";

import {StoreHelper} from "./StoreHelper";

import {markerCreated, markerRemoved} from "../actions/actionCreators";
import {getCurrentLayer, getLayers} from "../selectors/selectors";
import {ToasterHelper} from "./ToasterHelper";
import {Intent} from "@blueprintjs/core";
import {LayerService} from "../services/LayerService";

export class MapHelper {

    static _map;
    static _lastMarkers;

    static defaults = {
      mapId: 'map',
      lat: 51.505,
      lng: -0.09,
      zoom: 13,
      url: 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
      accessToken: 'pk.eyJ1IjoidmFkaW1rYSIsImEiOiJjamYxaTRsY28wMjl1MnpvNHVxMXZoeGJtIn0.xAtmvhfH6g73ViRhHZVExQ',
      attibution: 'Thank you for opportunity and great test task',
      maxZoom: 18,
      id: 'mapbox.streets'
    };

    static _lastLayer;

    static setLayer(layer) {
        MapHelper._lastLayer = layer;
    }

    static getLayer() {
        return MapHelper._lastLayer;
    }

    static initMap(params) {
        if (!MapHelper.getMap()) {
            MapHelper.setMap(L.map(params.mapId).setView([params.lat, params.lng], params.zoom));
        }

        L.tileLayer(params.url, {
            maxZoom: params.maxZoom,
            id: params.id,
            accessToken: params.accessToken
        }).addTo(MapHelper.getMap());

        this.getMap().on('click', function(e) {
            console.log('map clicked',e);
            const currentLayer = getCurrentLayer(StoreHelper.getState());
            if (!currentLayer) return ToasterHelper.show({intent: Intent.DANGER, message: `Ошибка: слой не выбран!`});
            StoreHelper.getStore().dispatch(markerCreated(e.latlng));
        });
    }

    static setMarkers(markers) {
        MapHelper._lastMarkers = markers;
    }

    static getMarkers() {
        return MapHelper._lastMarkers;
    }

    static drawMarkers() {
        const markers = MapHelper.getMarkers().map(marker => L.marker([marker.lat, marker.lng]));
        const lastLayer = MapHelper.getLayer();
        if (lastLayer) MapHelper.getMap().removeLayer(lastLayer);
        const layer = L.featureGroup(markers).addTo(MapHelper.getMap());
        MapHelper.setLayer(layer);
        layer.on('click', function(e) {
            console.log('layer clicked',e.propagatedFrom);
            StoreHelper.getStore().dispatch(markerRemoved(e.propagatedFrom._latlng));
        });
    }

    static init(params) {
        MapHelper.initMap(Object.assign({}, MapHelper.defaults, params ? params : {}));
        const currentLayer = getCurrentLayer(StoreHelper.getState());
        if (currentLayer) MapHelper.setMarkers(currentLayer.markers);

        StoreHelper.getStore().subscribe(function() {
            const currentLayer = getCurrentLayer(StoreHelper.getState());
            if (currentLayer) {
                if (MapHelper.getMarkers() !== currentLayer.markers) {
                    MapHelper.setMarkers(currentLayer.markers);
                    console.log('it is updated');
                    MapHelper.drawMarkers();
                }
            }

            else {
                MapHelper.setMarkers([]);
                console.log('it is updated');
                MapHelper.drawMarkers();
            }
        });


    }

    static setMap(map) {
        MapHelper._map = map;
    }

    static getMap() {
        return MapHelper._map;
    }



}