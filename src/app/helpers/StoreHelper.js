import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducer from "../reducers";


export class StoreHelper {
    static _store;

    static setStore(store) {
        StoreHelper._store = store;
    }

    static getStore() {
        return StoreHelper._store;
    }

    static getState() {
        return StoreHelper._store.getState();
    }

    static createStore() {

        const store = createStore(reducer, applyMiddleware(ReduxThunk));

        if (module.hot) {
            // Enable Webpack hot module replacement for reducers
            module.hot.accept('../reducers', () => {
                const nextRootReducer = require("../reducers");
                store.replaceReducer(nextRootReducer);
            });
        }

        StoreHelper.setStore(store);

        return store;
    }
}