import {getLayers} from "../selectors/selectors";
import {StoreHelper} from "../helpers/StoreHelper";

export class LayerService {

    static LAYER_KEY = 'layers';

    static init() {
        if (!localStorage.getItem(LayerService.LAYER_KEY)) {
            localStorage.setItem(LayerService.LAYER_KEY, JSON.stringify([]));
        }

    }

    static saveChanges(layers) {
        return new Promise(function(resolve, reject) {
            return setTimeout(function() {

                localStorage.setItem(LayerService.LAYER_KEY, JSON.stringify(layers));
                return resolve(layers);

            }, 500);
        });

    }

    static getLayers() {

        return new Promise(function(resolve, reject) {

            return resolve(JSON.parse(localStorage.getItem(LayerService.LAYER_KEY)).map(l => {
                l.current = false;
                return l;
            }));

        });

    }

    static removeLayer(layer) {
        return new Promise(function(resolve, reject) {
            const layers = JSON.parse(localStorage.getItem(LayerService.LAYER_KEY));

            const _layer = layers.find(l => l.id == layer.id);
            const index = layers.indexOf(_layer);

            const newLayers = [
                ...layers.slice(0, index),
                ...layers.slice(index+1)
            ];

            localStorage.setItem(LayerService.LAYER_KEY, JSON.stringify(newLayers));

            return setTimeout(() => resolve(layer.id), 500);

        });
    }

    static addLayer(layer) {
        return new Promise(function(resolve, reject) {

            const layers = JSON.parse(localStorage.getItem(LayerService.LAYER_KEY));

            layers.push(layer);

            layers.forEach((layer, i) => {
                layer.id = i; //так тоже делать не надо, понятно, почему, но это я получу с бекенда
            });

            localStorage.setItem(LayerService.LAYER_KEY, JSON.stringify(layers));

            return setTimeout(() => resolve(layer.id), 500);

        });
    }

}