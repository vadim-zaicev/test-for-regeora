import { createSelector } from 'reselect';

export function getLayers(state) {
    return state.App.layers;
}

export function isLoading(state) {
    return state.App.loading;
}

export function layerName(state) {
    return state.App.layerName;
}

export function layerAdding(state) {
    return state.App.layerAdding;
}

export const getMarkers = createSelector(
    getLayers,
    layers => {
        return layers.reduce(function(result, layer) {
            return result.length
                ? result
                : [].concat(layer.markers);
        }, []);
    }
);

export const getCurrentLayer = createSelector(
    getLayers,
    layers => {
        return layers.reduce(function(result, layer) {
            return result
                ? result
                : (layer.current ? layer : null);
        }, null);
    }
);

export function currentLayerSaved(state) {
    return state.App.currentLayerSaved;
}

