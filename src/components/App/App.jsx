import * as React from "react";
import { withRouter } from 'react-router';
import { connect } from "react-redux";

import { Route } from 'react-router';
import classNames from "classnames/bind";
import {HistoryHelper} from "../../app/helpers/HistoryHelper";

import {
    Button,
    Spinner,
    Classes,
    Intent,
    InputGroup,
    Icon
} from "@blueprintjs/core";

import {LayerService} from "../../app/services/LayerService";
import {MapHelper} from "../../app/helpers/MapHelper";

import {isLoading, getLayers, layerName, layerAdding, getMarkers, getCurrentLayer, currentLayerSaved } from "../../app/selectors/selectors";

import {layersLoaded, addLayer, layerNameChanged, layerSelected, saveChangesClicked, deleteLayerClicked, editModeToggled, layerUpdated} from "../../app/actions/actionCreators";

class App extends React.Component {

    componentWillMount() {
        LayerService.init();
        LayerService.getLayers()
        .then(layers => {
            console.log('layers', layers);
            this.props.layersLoaded(layers);
        });
    }

    componentDidMount() {

        MapHelper.init();

        HistoryHelper.setHistory(this.props.history);
    }

    getLayers() {
        return <div className="pt-button-group pt-vertical pt-minimal pt-fill">
            {this.props.layers.map((layer, i) => {
                const button = <div key={i} style={{display: "flex", alignItems: "center"}}>
                    <Button
                        className={layer.current ? 'pt-active' : null}
                        style={{width: '100%'}}
                        iconName={'layer'}
                        key={i} onClick={e => {
                        (this.props.currentLayerSaved)
                            ? this.props.layerSelected(layer)
                            : (confirm("Вы уверены, что хотите выбрать другой слой? Несохраненные изменения пропадут.")
                            ? this.props.layerSelected(layer)
                            : null);
                    }}>{layer.name}</Button>
                    <Icon iconName="edit" style={{cursor: "pointer"}}
                          onClick={e => this.props.editModeToggled(layer)}
                    />
                    <Icon iconName="cross" style={{cursor: "pointer", color: "red"}}
                        onClick={e => confirm(`Точно хотите удалить слой ${layer.name}?`) ? this.props.deleteLayerClicked(layer) : null}
                    />
                </div>;

                const editInput = <InputGroup
                    className={Classes.LARGE}
                    leftIconName="edit"
                    type="text"
                    key={i}
                    value={layer.name}
                    onChange={e => this.props.layerUpdated({layer: layer, change: {name: e.target.value}})}
                    disabled={this.props.changesUpdating}
                />;

                return layer.editMode
                        ? editInput
                        : button;
            })}




        </div>
    }

    getSpinner() {
        return <div style={{textAlign:"center"}}>
            <Spinner
                intent={Intent.PRIMARY}
                className={Classes.SMALL}
            />
        </div>;
    }

    render() {

        return <div>
                <div className={'map-container'}>

                    <div id="controls">
                        {
                            this.props.isLoading
                                ? this.getSpinner()
                                : this.getLayers()
                        }
                    </div>

                    <div id="map">

                    </div>

                </div>

                <div>
                    <Button
                        onClick={e => this.props.saveChangesClicked(this.props.layers)}
                        style={{marginLeft: "20px", minWidth: '160px'}}
                        iconName={'save'}
                        intent={Intent.SUCCESS}
                    >Сохранить изменения</Button>

                </div>

                <div className={'bottom-panel'}>
                    <InputGroup
                        className={Classes.LARGE}
                        leftIconName="layers"
                        placeholder="Введите название слоя"
                        type="text"
                        value={this.props.layerName}
                        onChange={e => this.props.layerNameChanged(e.target.value)}
                        disabled={this.props.layerAdding}
                    />

                    <Button
                        onClick={e => this.props.addLayer(
                            {name: this.props.layerName, markers: []}
                        )}
                        disabled={this.props.layerAdding}
                        style={{marginLeft: "20px", minWidth: '160px'}}
                        iconName={'add'}
                        intent={Intent.SUCCESS}
                    >Добавить слой</Button>
                </div>

        </div>;
    }
}

const mapStateToProps = (state, props) => {

    const currentLayer = getCurrentLayer(state);

    return {
        isLoading: isLoading(state),
        layers: getLayers(state),
        layerName: layerName(state),
        layerAdding: layerAdding(state),
        currentLayer: getCurrentLayer(state),
        markers: getMarkers(state),
        currentLayerSaved: currentLayerSaved(state),
        changesUpdating: state.App.changesUpdating,
    };
};


export default withRouter(connect(mapStateToProps, {
    layersLoaded,
    addLayer,
    layerNameChanged,
    layerSelected,
    saveChangesClicked,
    deleteLayerClicked,
    editModeToggled,
    layerUpdated,
})(App));

