const path = require('path');
const fs = require('fs');
const replace = require("replace");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const WebpackOnBuildPlugin = require('on-build-webpack');
const ncp = require('ncp').ncp;

const stylusLoader = ExtractTextPlugin.extract({
    fallback: "style-loader",
    use: "css-loader!stylus-loader",
});

const sassLoader = ExtractTextPlugin.extract({
    fallback: "style-loader",
    use: "css-loader!sass-loader",
});

const PRODUCTION = process.env.NODE_ENV === 'production';

const entry = PRODUCTION
    ? [ './src/index.js' ]
    : [
        './src/index.js',
        'webpack/hot/dev-server',
        'webpack-dev-server/client?http://localhost:8080'
    ];

const plugins = PRODUCTION
    ? [
        new webpack.optimize.UglifyJsPlugin({
            comments: false,
            mangle: false,
            compress: {
                warnings: true
            }
        })
      ]
    : [ new webpack.HotModuleReplacementPlugin(), new webpack.NamedModulesPlugin() ];

plugins.push(
    new webpack.DefinePlugin({
        PRODUCTION: PRODUCTION,
        DEVELOPMENT: !PRODUCTION
    }),
    new ExtractTextPlugin("main.min.css")
);

if (PRODUCTION) {

    plugins.push(
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorOptions: { discardComments: {removeAll: true } },
            canPrint: true
        })
    );

    plugins.push(
        new CopyWebpackPlugin([

        ])
    );

    plugins.push(
        new WebpackOnBuildPlugin(function(stats) {
            console.log('finished build', __dirname);
        })
    );
}

const loaders = [
    {
        test: /node_modules[\\\/]vis[\\\/].*\.js$/,
        loader: 'babel-loader',
        query: {
            cacheDirectory: true,
            presets: [ "babel-preset-es2015" ].map(require.resolve),
            plugins: [
                "transform-es3-property-literals", // #2452
                "transform-es3-member-expression-literals", // #2566
                "transform-runtime" // #2566
            ]
        }
    },

    {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: '/node_modules/',
        query: {
            presets: ['react', 'es2015', 'stage-2']
        }
    },
    {
        test: /\.styl$/,
        loader: PRODUCTION ? stylusLoader : [ 'style-loader', 'css-loader', 'stylus-loader' ]
    },
    {
        test: /\.scss$/,
        loader: PRODUCTION ? sassLoader : ['style-loader', 'css-loader', 'sass-loader' ]
    },
    {
        test: /\.css$/,
        loaders: [ 'style-loader', 'css-loader' ]
    },
    {
        test: /\.(png|woff|woff2|eot|ttf|svg|gif)$/,
        loader: 'file-loader',
        options: {
            name: "/fonts/[name].[ext]"
         },
    },
];


module.exports = {
    devtool: 'source-map',
    entry: entry,
    plugins: plugins,
    module: {
        unknownContextRegExp: /$^/,
        unknownContextCritical: false,
        loaders: loaders
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: PRODUCTION ? '' : '/dist/',
        filename: PRODUCTION ? 'bundle.min.js' : 'bundle.js'
    },
    externals: {
        
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    }
};